import { useState, useEffect } from 'react';
import { useAuth } from '../contexts/AuthContext';

import { SpotifyPlaylistType } from '../data/types/PlaylistTypes';
import { getAllPlaylists } from '../data/api/SpotifyApi';

type PlaylistsType = {
  playlists: Array<SpotifyPlaylistType>;
}

const useSpotify = () => {
  const { spotifyToken } = useAuth()!;
  const [playlists, setPlaylists] = useState<SpotifyPlaylistType[]>([]);

  const getPlaylists = async () => {
    try {
      const res:Array<SpotifyPlaylistType> = await getAllPlaylists(spotifyToken.access_token);
      setPlaylists(res);
    } catch (error) {
      console.error(error);
    }
  }
}

export default useSpotify;