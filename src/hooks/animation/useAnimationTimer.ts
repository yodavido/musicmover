import { useState, useEffect } from 'react';

const useAnimationTimer = (duration: number, delay: number, callback: Function | undefined) => {
  const [elapsed, setTime] = useState(0);

  useEffect(() => {
    let animationFrame: number, 
        timerStop: NodeJS.Timeout, 
        start: number;

    function onFrame() {
      setTime(Date.now() - start);
      loop();
    }

    function loop() {
      animationFrame = requestAnimationFrame(onFrame);
    }

    function onStart() {
      timerStop = setTimeout(() => {
        cancelAnimationFrame(animationFrame);
        setTime(Date.now() - start);
        if (callback) callback();
      }, duration);

      start = Date.now();
      loop();
    }

    const timerDelay: NodeJS.Timeout = setTimeout(onStart, delay);

    return () => {
      clearTimeout(timerStop);
      clearTimeout(timerDelay);
      cancelAnimationFrame(animationFrame);
    }
  }, [duration, delay, callback]);

  return elapsed;
}

export default useAnimationTimer;