import { animationEquations as equations } from '../../data/enums';
import useAnimationTimer from './useAnimationTimer';

const easing = (equation: string): (n: number) => number => {
  switch (equation) {
    case equations.LINEAR:
      return (n: number) => n;
    case equations.ELASTIC:
      return (n: number) => n * 33 * n * n * n * n - 106 * n * n * n + 126 * n * n - 67 * n + 15;
    case equations.EXPO_IN:
      return (n: number) => Math.pow(2, 10 * (n - 1));
    case equations.QUAD_OUT:
      return (n: number) => n * (2 - n)
    default:
      return (n: number) => n;
  }
}

const useAnimation = (equation: string = 'linear', duration: number = 500, delay: number = 0, reverse: boolean = false, callback: Function | undefined = undefined) => {
  const elapsed = useAnimationTimer(duration, delay, callback);
  const n = Math.min(1, elapsed/duration);
  return easing(equation)(reverse ? 1 - n : n);
}

export default useAnimation;