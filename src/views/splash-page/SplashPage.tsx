import React, { useEffect } from 'react';
import { useLocation } from 'react-router-dom';

import logo from '../../assets/images/logo.png';
import Button from '../../components/button/Button';

import { useAuth } from '../../contexts/AuthContext';

import { getQueryParams, QueryResultType } from '../../utils/RouterUtils';

import './splash-page.css';

const SplashPage = () => {
  const { spotifyToken, requestSpotifyToken } = useAuth()!;

  const location = useLocation();
  const queryParams: QueryResultType = getQueryParams(location.search);
  
  useEffect(() => {
    if (queryParams.state === 'service-spotify' && spotifyToken.refresh_token === '') {
      requestSpotifyToken(queryParams.code, false);
    }
  }, [spotifyToken, queryParams, requestSpotifyToken]);

  const serviceClickHandler = (service: string) => {
    window.location.href = `http://localhost:4000/v1/auth/${service}-code-request`;
  }

  return (
    <div className="splash">
      <div className="splash-intro">
        <img src={logo} alt=""/>
        <h1>MusicMover</h1>
        <p>Welcome to MusicMover. 
        Don’t want to rebuild the playlists and library you’ve painstakingly put together.
        MusicMover gives you the ability to list out your library and import it from it’s old home
        to it’s new residence.</p>
      </div>
      <div className="splash-actions">
        <p className="splash-actions-desc">To get started connect with your music’s old home.</p>
        <Button className="dangerbg" label="Connect With Apple Music" onClick={() => console.log('connect with apple')}/>
        <p className="splash-actions-or">or</p>
        <Button className="successbg" label="Connect With Spotify Music" onClick={() => serviceClickHandler('spotify')}/>
      </div>
    </div>
  );
}

export default SplashPage;