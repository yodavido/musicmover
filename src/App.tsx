import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';

import { AuthContextProvider } from './contexts/AuthContext';
import { NotificationContextProvider } from './contexts/NotificationContext';

import ProtectedRoute from './utils/ProtectedRoute';
import { readFromLocalStorage } from './utils/LocalStorageUtils';

import Notifications from './components/notifications/Notifications';

import SplashPage from './views/splash-page/SplashPage';
import Library from './views/library/Library';

import './App.css';

const App = () => {
  return (
    <AuthContextProvider>
      <NotificationContextProvider>
        <div className="App">
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={SplashPage} />
              <ProtectedRoute path="/library/:service/:section" component={Library} user={readFromLocalStorage('user')}  />
            </Switch>
          </BrowserRouter>
          <Notifications />
        </div>
      </NotificationContextProvider>
    </AuthContextProvider>
  );
}

export default App;
