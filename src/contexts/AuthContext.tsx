import React, { ReactNode, createContext, useContext, useState } from 'react';

import { SpotifyTokenType } from '../data/types/TokenTypes';
import { spotifyTokenRequest } from '../data/api/AuthApi';

type ContextType = {
  spotifyToken: SpotifyTokenType;
  requestSpotifyToken: (code: string, isRefresh: boolean) => void;
  requestAppleToken: () => void;
}

type Props = {
  children: ReactNode
}

export const AuthContext = createContext<ContextType | undefined>(undefined);

export const AuthContextProvider = ({ children }: Props) => {
  const [spotifyToken, setSpotifyToken] = useState({
    access_token: '',
    expires_in: 0,
    refresh_token: '',
    scope: '',
    token_type: '' 
  });

  const requestSpotifyToken = async (authCode: string) => {
    const token: SpotifyTokenType = await spotifyTokenRequest(authCode);
    setSpotifyToken(token);
    window.location.href = 'http://localhost:3000/library/spotify/playlists';
  }
  const refreshSpotifyToken = async () => {
    const token: SpotifyTokenType = await spotifyTokenRequest(spotifyToken, true);
    setSpotifyToken(token);
  }
  const requestAppleToken = () => console.log('requesting apple token');

  return (
    <AuthContext.Provider value={{spotifyToken, requestSpotifyToken, requestAppleToken}}>
      {children}
    </AuthContext.Provider>
  )
}

export const useAuth = () => useContext(AuthContext);