import React, { ReactNode, createContext, useContext } from 'react';

interface Props {
  children: ReactNode;
}

export const LibraryContext = createContext<undefined>(undefined);

export const LibraryContextProvider = ({children}: Props) => {
  return (
    <LibraryContext.Provider value={undefined}>
      {children}
    </LibraryContext.Provider>
  );
}

export const useLibrary = () => useContext(LibraryContext);