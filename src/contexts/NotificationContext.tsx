import React, { ReactNode, createContext, useContext, useReducer, Dispatch } from 'react';

import { NotificationType } from '../data/types/NotificationType';
import { notificationReducerActions as actions } from '../data/enums';

interface Props {
  children: ReactNode;
}

interface IAction {
  type: string;
}

interface AddAction extends IAction {
  payload: NotificationType;
}

interface RemoveAction extends IAction {
  payload: string
}

type ActionType = AddAction | RemoveAction;

const isAddAction = (action: IAction): action is AddAction => action.type === actions.ADD;
const isRemoveAction = (action: IAction): action is RemoveAction => action.type === actions.REMOVE;

type ContextType = {
  notifications: Array<NotificationType>;
  notificationsDispatch: Dispatch<ActionType>;
}

const initialState: Array<NotificationType> = [];

export const NotificationContext = createContext<ContextType | undefined>(undefined);

export const notificationReducer = (state:Array<NotificationType>, action: IAction) => {
  if (isAddAction(action)) {
    return [
      ...state,
      action.payload
    ];
  }
  if (isRemoveAction(action)) {
    return state.filter(notif => notif.id !== action.payload);
  }

  return state;
}

export const NotificationContextProvider = ({children}: Props) => {
  const [notifications, notificationsDispatch] = useReducer(notificationReducer, initialState);

  return (
    <NotificationContext.Provider 
      value={{notifications, notificationsDispatch}}>
      {children}
    </NotificationContext.Provider>
  );
}

export const useNotification = () => useContext(NotificationContext);