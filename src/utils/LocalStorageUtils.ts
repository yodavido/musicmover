interface IStorageItem {
  key: string;
  val: any;
}

export class StorageItem {
  key: string;
  val: any;

  constructor(item: IStorageItem) {
    this.key = item.key;
    this.val = item.val;
  }
}

export const writeToLocalStorage = (item: StorageItem):void => {
  localStorage.setItem(item.key, JSON.stringify(item.val));
}

export const readFromLocalStorage = (key: string):StorageItem =>  {
  const val = localStorage.getItem(key)!;
  return new StorageItem({key, val: JSON.parse(val)});
}