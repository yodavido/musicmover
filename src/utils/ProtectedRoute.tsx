import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';

import { readFromLocalStorage } from '../utils/LocalStorageUtils';

type RouteProps = {
  component: any;
  user: any;
  [key: string]: any;
}

const ProtectedRoute = ({component,  ...rest}: RouteProps) => {
  return (
    <Route {...rest} render={props => {
      const user = readFromLocalStorage(props.match.params.service);
      if(user) {
        return React.createElement(component, {...rest, ...props});
      } else {
        return <Redirect to={{pathname: "/", state: { from: props.location}}} />
      }
    }} />
  );
}

export default ProtectedRoute;