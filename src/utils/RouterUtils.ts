export interface QueryResultType {
  [key: string]: any
}

export const getQueryParams = (search: string): Object =>  {
  const queryParams = search.split('&');
  const result: QueryResultType = {};
  for(let i = 0; i < queryParams.length; i++) {
    const splitParam = queryParams[i].split('=');
    let key = splitParam[0].startsWith('?') ? splitParam[0].slice(1, splitParam[0].length) : splitParam[0];
    let val = splitParam[1];
    result[key] = val;
  }

  return result;
}