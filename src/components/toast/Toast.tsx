import React from 'react';
import CheckCircleRoundedIcon from '@material-ui/icons/CheckCircleRounded';
import ReportProblemRoundedIcon from '@material-ui/icons/ReportProblemRounded';
import ErrorRoundedIcon from '@material-ui/icons/ErrorRounded';
import CancelRoundedIcon from '@material-ui/icons/CancelRounded';

import { notificationReducerActions as actions } from '../../data/enums';
import { useNotification } from '../../contexts/NotificationContext';
import useAnimation from '../../hooks/animation/useAnimation';

import './toast.css';

interface ToastProps {
  id: string,
  index: number;
  level?: string;
  label?: string;
  message?: string;
}

interface ToastIconProps {
  level: string;
}

const toastIcon = ({level}: ToastIconProps) => {
  switch (level) {
    case "success":
      return <CheckCircleRoundedIcon fontSize={'small'} style={{marginRight: '16px'}} />
    case "warning":
      return <ReportProblemRoundedIcon fontSize={'small'} style={{marginRight: '16px'}} />
    case "danger":
      return <ErrorRoundedIcon fontSize={'small'} style={{marginRight: '16px'}} />
  }
}
const Toast = ({id, level, label, message}:ToastProps) => {
  const { notificationsDispatch } = useNotification()!;
  const animationIn = useAnimation('expoIn', 350, 0);
  setTimeout(() => notificationsDispatch({type: actions.REMOVE, payload: id}), 5000);
  //const animationOut = useAnimation('quadOut', 500, 10000, true, () => notificationsDispatch({type: actions.REMOVE, payload: id}));
  return (
    <div id={`toast-${id}`} className={`toast ${level ? level : ''}`} style={{
      right: animationIn * 320 - 320
    }}>
      <div className="toast-inner">
        {level && toastIcon({level})}
        {label && <span className="label">{label}</span>}
        {message && <span className="message">{message}</span>}
      </div>
      <span className="toast-close" onClick={() => notificationsDispatch({type: actions.REMOVE, payload: id})}>
        <CancelRoundedIcon fontSize={'small'} />
      </span>
    </div>
  );
}

export default Toast;