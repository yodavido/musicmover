import React from 'react';

import './button.css';

type ButtonProps = {
  className?: string;
  icon?: string;
  label: string;
  onClick: () => void;
}

const Button = ({className, icon, label, onClick}: ButtonProps) => {
  return (
    <div className={`btn ${ className ? className : ''}`} onClick={onClick}>
      {label}{icon}
    </div>
  )
}

export default Button;