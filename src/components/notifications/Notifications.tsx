import React from 'react';

import { useNotification } from '../../contexts/NotificationContext';

import Toast from '../toast/Toast';

import './notifications.css';

const Notifications = () => {
  const { notifications } = useNotification()!;

  return (
    <div className="notifications">
      {notifications.map((notification, i) => <Toast key={`toast-${i}`} {...notification} index={i} />)}
    </div>
  )
}

export default Notifications;