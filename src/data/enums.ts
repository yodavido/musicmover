export const animationEquations = {
  LINEAR: 'linear',
  ELASTIC: 'elastic',
  EXPO_IN: 'expoIn',
  QUAD_OUT: 'quadOut'
};

export const notificationReducerActions = {
  ADD: 'add',
  REMOVE: 'remove',
  REMOVE_ALL: 'remove_all'
}