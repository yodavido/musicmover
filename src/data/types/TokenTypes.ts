export type SpotifyTokenType = {
  access_token: string; //api access token
  expires_in: number; //time period that the access token is valid
  refresh_token: string; //used to request a new auth_token after auth_token expires
  scope: string; //a space delimited list of scopes that the access_token has been granted
  token_type: string; //how access token can be used. Always 'Bearer'
}

export type AppleTokenType = {
  access_token: string; //api access token
  expires_in: number; //time period that the access token is valid
  refresh_token: string; //used to request a new auth_token after auth_token expires
  scope: string; //a space delimited list of scopes that the access_token has been granted
  token_type: string; //how access token can be used. Always 'Bearer'
}