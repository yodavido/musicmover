export type SpotifyArtistType = {
  id: string;
  name: string;
  externalUrl: string;
}

export type SpotifyTrackType = {
  id: string;
  name: string;
  artists: Array<SpotifyArtistType>;
  previewUrl: string;
  trackUrl: string;
}

export type SpotifyPlaylistType = {
  id: string;
  name: string;
  description: string;
  tracksTotal: number;
  externalUrl: string;
  tracks?: Array<SpotifyTrackType>
}