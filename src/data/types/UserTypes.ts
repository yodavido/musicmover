type SpotifyImageType = {
  height: string;
  url: string;
  width: string;
}
export type SpotifyUserType = {
  id: string;
  spotify: string;
  images: Array<SpotifyImageType>;
}
export type UserType = {
  spotifyUser: SpotifyUserType;
  appleUser: string;
}