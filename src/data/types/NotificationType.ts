export type NotificationType = {
  id: string;
  level?: string;
  icon?: string;
  label?: string;
  message?: string;
}