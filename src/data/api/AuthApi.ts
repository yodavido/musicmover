import axios from 'axios';
import { SpotifyTokenType } from "../types/TokenTypes";
import { writeToLocalStorage } from '../../utils/LocalStorageUtils';


export const spotifyTokenRequest = async (token: string | SpotifyTokenType, isRefresh: boolean = false): Promise<SpotifyTokenType> => {
  try {
    let data = isRefresh ? token : {authCode: token};
    const res = await axios({
      method: 'post',
      url: `http://localhost:4000/v1/auth/spotify-token-${isRefresh ? 'refresh' : 'request'}`,
      data,
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });

    writeToLocalStorage({key: 'spotifyUser', val: res.data.user});

    return res.data.token;
  } catch (error) {
    return error;
  }
}