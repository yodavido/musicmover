import axios from 'axios';

import { SpotifyPlaylistType } from '../types/PlaylistTypes';

export const getAllPlaylists = async (token: string): Promise<Array<SpotifyPlaylistType>> => {
  try {
    const res = await axios({
      method: 'get',
      url: 'http://localhost:4000/v1/spotify/playlists',
      data: {
        accessToken: token
      },
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      }
    });

    return res.data;
  } catch (error) {
    return error;
  }
}